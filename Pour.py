import math
from collections import deque

class state:
    def __init__(self, a, b, steps):
        self.tup = (a,b) #[0] = a, [1] = b
        self.steps = steps

def Solve(a, b, c):
    #setup var for consistency
    if a > b:
        temp = a
        a = b
        b = temp

    ForwardRemainder = b%a
    difference = b-a
    BackwardRemainder = b - difference

    #base case
    if a is c or b is c:
        return 1
    #error case
    if a is b or c is 0 or (c > a and c > b):
        return -1
    #base case2
    if a == 1 or b == 1:
        return 2*c

    previouslyVisited = [(0,0)]
    #possibleAnswers = []
    runningList = deque([state(a, 0, 1), state(0, b, 1)])

    while len(runningList) > 0:
        currentState = runningList.popleft()
        previouslyVisited.append(currentState.tup)

        #print(previouslyVisited)
        #1. Fill A
        if currentState.tup[0] is not a:
            newState = state(a, currentState.tup[1], currentState.steps+1)
            if newState.tup not in previouslyVisited:
                runningList.append(newState)

        #2. Fill B
        if currentState.tup[1] is not b:
            newState = state(currentState.tup[0], b, currentState.steps+1)
            if newState.tup not in previouslyVisited:
                runningList.append(newState)

        #3. Empty A
        if currentState.tup[0] > 0:
            newState = state(0, currentState.tup[1], currentState.steps+1)
            if newState.tup not in previouslyVisited:
                runningList.append(newState)
                
        #4. Empty B
        if currentState.tup[1] > 0:
            newState = state(currentState.tup[0], 0, currentState.steps+1)
            if newState.tup not in previouslyVisited:
                runningList.append(newState)
                
        #5. A -> B , small to large
        if currentState.tup[0] is not 0:
            AB = currentState.tup[0] + currentState.tup[1]
            #print("AB: " + str(AB))
            #A overflows B
            if AB > b:
                remainder = AB - b
                if remainder is c:
                    #possibleAnswers.append(currentState.steps+1)
                    return currentState.steps + 1
                
                newState = state(remainder, b, currentState.steps+1)
                if newState.tup not in previouslyVisited:
                    runningList.append(newState)
            #A fully pours to B
            elif AB < b:
                if AB is c:
                    #possibleAnswers.append( currentState.steps+1 )
                    return currentState.steps + 1
                
                newState = state(0, AB, currentState.steps+1)
                if newState.tup not in previouslyVisited:
                    runningList.append(newState)
            #ignore AB = b, as one of initial states

        #6. B -> A, large to small
        if currentState.tup[1] is not 0:
            AB = currentState.tup[0] + currentState.tup[1]
            #A filled
            if AB > a:
                remainderB = currentState.tup[1] - (a - currentState.tup[0])
                #print("remainderB: " + str(remainderB))
                if remainderB is c:
                    #possibleAnswers.append( currentState.steps+1 )
                    return currentState.steps + 1
                newState = state(a, remainderB, currentState.steps+1)
                if newState.tup not in previouslyVisited:
                        runningList.append(newState)
            #A not completely filled
            elif AB < a:
                newState = state(AB, 0, currentState.steps+1)
                if newState.tup not in previouslyVisited:
                        runningList.append(newState)
    if len(possibleAnswers) is not 0:
        return min (possibleAnswers)
    else:
        return -1
    
inputList =[]
output = []
cases = int(input(""))
for i in range(cases):
    caseList = []
    for j in range (3):
        caseList.append(int(input("")))
    inputList.append(caseList)

#print(inputList)
for caseList in inputList:
    output.append(Solve(caseList[0], caseList[1], caseList[2]))

print("")
for Output in output:
    print(str(Output))
